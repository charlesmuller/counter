<?php

require_once __DIR__ . '/../Data/DataBaseConnection.php';
require_once 'IndexController.php';
if (!$_SERVER['REQUEST_METHOD'] === 'POST') {
    die('deu ruim');
}
if (isset($_POST['peso'])) {
    $objIndex   = new IndexController();
    $dbConnection = new DataBaseConnection();

    $peso = $_POST['peso'];
    $peso = htmlspecialchars($peso, ENT_QUOTES, 'UTF-8');

    try {
        $sql = "INSERT INTO dadosdepeso (peso) VALUES (:peso)";
        $stmt = $dbConnection->getConnection()->prepare($sql);

        $stmt->bindParam(':peso', $peso, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return $objIndex->indexAction($userId);
        } else {
            echo "Erro ao inserir os dados.";
        }
    } catch (PDOException $e) {
        echo "Erro na conexão com o banco de dados: " . $e->getMessage();
    }

    $dbConnection->closeConnection();
}
