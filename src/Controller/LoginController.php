<?php

include_once('../../src/Data/DataBaseConnection.php');
include_once('IndexController.php');

session_start();
// // Verifique se o cookie 'PHPSESSID' está definido na requisição
// if (isset($_COOKIE['PHPSESSID'])) {
//     // Use o valor do cookie para definir a sessão
//     session_id($_COOKIE['PHPSESSID']);
// }

// $_SESSION['usuario'] = 'joao.das.coves';

// // echo "<pre>";
// // var_dump($_SESSION);die;

// // Para salvar as mudanças na sessão (garantir que o cookie seja enviado de volta para o cliente)
// session_write_close();


$recaptcha_secret = "6Ld8MuUnAAAAAIa4mRH7Ky345rNgGKsT2fCQh7ks";
$response = $_POST['g-recaptcha-response'];

$verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$recaptcha_secret&response=$response");
$captcha_success = json_decode($verify);

$objConn    = new DatabaseConnection();
$objIndex   = new IndexController();

//var_dump($verify);
if ($captcha_success->success) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!isset($_POST["email"]) || !isset($_POST["password"])) {
            echo "Campos de usuário e senha são obrigatórios 123.";
            return;
        }

        if (empty($_POST["email"]) || empty($_POST["password"])) {
            echo "Campos de usuário e senha não podem estar vazios.";
            return;
        }

        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            echo "Formato de e-mail inválido.";
            return;
        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        $userId = validaLogin($email, $password, $objConn);

        return $objIndex->indexAction($userId);
    }
} else {
    // O reCAPTCHA não foi resolvido corretamente
    echo "Por favor, prove que você não é um robô.";
}

function validaLogin($email, $password, $objConn)
{
    $sql = "SELECT id, email, password FROM usuarios WHERE email = :email";
    $conn = $objConn->getConnection();

    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->execute();

    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $_SESSION = $result;
    $userId = $_SESSION['id'];

    if (!$result) {
        die("LOOK AT YOU!!!!!!!!");
    }

    if ($password != $result['password'] || $result['email'] != $email) {
        die("FAKE NATTYYYYYY!!!!");
    }
    return $userId;
}
