<?php

require_once __DIR__ . '/../../vendor/autoload.php';

class DatabaseConnection {
    private $servername;
    private $username;
    private $dbname;
    private $password;
    private $conn;

    public function __construct() 
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $env = $dotenv->load();
        $this->servername = $env["DB_HOST"];
        $this->username   = $env["DB_USERNAME"];
        $this->dbname     = $env["DB_DATABASE"];
        $this->password   = $env["DB_PASSWORD"];
    
        if (!$this->servername || !$this->username || !$this->dbname || !$this->password) {
            die("As variáveis de ambiente não estão definidas corretamente.");
        }

        $this->openConnection();
    }

    private function openConnection() 
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception("Erro na conexão: " . $e->getMessage());
        }
    }

    public function getConnection() 
    {
        return $this->conn;
    }

    public function closeConnection() 
    {
        $this->conn = null;
    }

    public function query($sql) 
    {
        return $this->conn->query($sql);
    }
}

// // Exemplo de uso
// $dbConnection = new DatabaseConnection();
// $conn = $dbConnection->getConnection();

// // Agora você pode usar a conexão $conn para executar consultas e interagir com o banco de dados

// // Não esqueça de fechar a conexão quando terminar
// $dbConnection->closeConnection();
// ?>
